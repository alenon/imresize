package lenon.imresize

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class Main extends Application {

    static Stage primaryStage
    static final ImageProcessor imageProcessor = new ImageProcessor()

    @Override
    void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage

        Parent root = FXMLLoader.load(getResource("mainWindow.fxml"))
        primaryStage.setTitle("ImageResizer")
        primaryStage.setScene(new Scene(root, 700, 400))
        primaryStage.show()
    }

    static void main(String... args) {
        Application.launch(Main.class, args)
    }

    private URL getResource(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader()
        classLoader.getResource(fileName)
    }
}
