package lenon.imresize

import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.filters.Canvas
import net.coobird.thumbnailator.geometry.Position
import net.coobird.thumbnailator.name.Rename

import java.awt.*

/**
 * Created by yevdoa on 05/03/2017.
 */
class ImageProcessor {

    void run(String src, String dst, Color color = null) {
        final File folder = new File(src)
        final File destinationDir = new File(src + dst)
        if(!destinationDir.exists()) {
            destinationDir.mkdir()
        }

        if(!color) {
            color = new Color(0,0,0, 1)
        }

        File[] list = folder.listFiles().findAll { it.path ==~ /(?i).*(jpg|png|jpeg)$/ }
        Canvas canvas = new Canvas(1024, 1024, new Position() {
            @Override
            Point calculate(int enclosingWidth, int enclosingHeight, int width, int height, int insetLeft, int insetRight, int insetTop, int insetBottom) {
                Point point = new Point(0, 0)
                if (width > height) {
                    int delta = width - height
                    point.y = delta / 2
                } else if (width < height) {
                    int delta = height - width
                    point.x = delta / 2
                }

                point
            }
        }, color)

        Thumbnails.of(list)
                .size(1024, 1024)
                .addFilters([canvas])
                .outputFormat("png")
                .toFiles(destinationDir, Rename.NO_CHANGE)
    }
}
