package lenon.imresize

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.DirectoryChooser

class Controller {

    @FXML
    TreeView<String> treeView

    @FXML
    Button sourceBtn

    @FXML
    Button runBtn

    @FXML
    ImageView imageView

    @FXML
    ColorPicker colorPicker

    File sourceFolder

    @FXML
    void initialize() {
        sourceBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            void handle(ActionEvent e) {
                DirectoryChooser dc = new DirectoryChooser()
                dc.initialDirectory = new File(System.getProperty("user.home"))
                sourceFolder = dc.showDialog(Main.primaryStage)
                runBtn.disable = true

                if (sourceFolder == null || !sourceFolder.isDirectory()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR)
                    alert.headerText = "Could not open directory"
                    alert.contentText = "The file is invalid."
                    alert.showAndWait()
                } else {
                    runBtn.disable = false
                    treeView.setRoot(getNodesForDirectory(sourceFolder, 0))
                    treeView.root.expanded = true
                }
            }
        })

        runBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            void handle(ActionEvent e) {
                if (!sourceFolder) {
                    return
                }

                java.awt.Color color = new java.awt.Color(
                        colorPicker.value.red as float,
                        colorPicker.value.green as float,
                        colorPicker.value.blue as float,
                        colorPicker.value.opacity as float)


                Main.imageProcessor.run(treeView.root.value, "/processed", color)
                treeView.setRoot(getNodesForDirectory(sourceFolder, 0))
                treeView.root.expanded = true
            }
        })

        treeView.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<TreeItem<String>>() {

            @Override
            void changed(
                    ObservableValue<? extends TreeItem<String>> observable,
                    TreeItem<String> old_val, TreeItem<String> selectedItem) {

                loadImage(getPath(selectedItem))
            }

        })
    }

    private TreeItem<String> getNodesForDirectory(File directory, int level) {
        if (level > 10) {
            return null
        }

        TreeItem<String> root
        if (level == 0) {
            root = new TreeItem<String>(directory.absolutePath)
        } else {
            root = new TreeItem<String>(directory.name)
        }

        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                root.getChildren().add(getNodesForDirectory(file, ++level))
            } else {
                TreeItem<String> item = new TreeItem<String>(file.name)
                root.getChildren().add(item)
            }
        }

        return root
    }

    private void loadImage(String path) {
        File file = new File(path)
        Image image = new Image(file.toURI().toString())
        imageView.image = image
    }

    private String getPath(TreeItem<String> item) {
        if (item == null) {
            return ""
        }

        if (item.parent == null) {
            return item.value
        }

        return getPath(item.parent) + "/" + item.value
    }
}
